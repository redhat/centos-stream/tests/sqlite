# SQLite tests repository
SQLite has a very extensive upstream test suite as is. It is automatically run during each build and tests functionality. [Upstream documentation](https://www.sqlite.org/testing.html)

Due to this there is no need to once again test the SQLite itself downstream, but since SQLite is mainly a library and is being used by extensive number of applications, it is preferable that we test new builds with *at least* the core applications on the system.

All tests in this repository should have a valid description that will fully explain the background and reasoning behind the test. A link to a public thread discussing the issue is sufficient.

