#!/bin/bash
# vim: dict=/usr/share/rhts-library/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/sqlite/Regression/bz428153-segfault-using-EXPLAIN
#   Description: Test for bz428153 (segfault using EXPLAIN)
#   Author: Matej Susta <msusta@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2009 Red Hat, Inc. All rights reserved.
#   
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#   
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include beakerlib environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="sqlite"

rlJournalStart
    rlPhaseStartSetup Setup
	rlAssertRpm $PACKAGE
        rlRun "TmpDir=\`mktemp -d\`" 0 "Creating tmp directory"
	rlRun "VER=`sqlite3 -version | cut -d' ' -f 1`" 0 "Checking SQLite version"		# Check for version
    rlPhaseEnd

    rlPhaseStartTest Testing
        rlAssertExists $TmpDir
        rlRun "sqlite3 $TmpDir/test.db 'create table foo (bar varchar(10));'" 0 "Creating DB and test table"		# Just create a db with dumb table
	rlRun "sqlite3 $TmpDir/test.db 'explain select * from foo;' >$TmpDir/output" 0 "Executing EXPECT command"	# Segfault reproducer
    rlPhaseEnd

    rlPhaseStartCleanup Cleanup
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
