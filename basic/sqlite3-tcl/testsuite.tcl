package require sqlite3

sqlite3 opendb "database.db"

opendb eval "CREATE TABLE table_a (
    fooa INTEGER PRIMARY KEY,
    bara TEXT NOT NULL
);"

opendb eval "CREATE TABLE table_b (
    foob INTEGER PRIMARY KEY,
    barb TEXT NOT NULL
);"

set tableNames [opendb eval {SELECT tbl_name FROM sqlite_master}]
puts "Check tables exist"
puts $tableNames
if {$tableNames == "table_a table_b"} {
    puts "..PASS"
} else {
    puts "..FAIL"
    exit 1
}

opendb eval "INSERT INTO table_a (fooa, bara)
VALUES (1, 'test a test');"

opendb eval "INSERT INTO table_a (fooa, bara)
VALUES (2, 'test b test');"

puts "Check data exist"
opendb eval "SELECT * FROM table_a WHERE fooa = 1" values {
    set columnNames $values(*)
    puts $columnNames
    if {$columnNames == "fooa bara"} {
        puts "..PASS"
    } else {
        puts "..FAIL"
        exit 1
    }

    puts $values(bara)
    if {$values(bara) == "test a test"} {
        puts "..PASS"
    } else {
        puts "..FAIL"
        exit 1
    }
}