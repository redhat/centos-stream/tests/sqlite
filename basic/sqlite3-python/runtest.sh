#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/sqlite3/Sanity/basic
#   Description: Basic test for python and sqlite3 DB
#   Author: Branislav Nater <bnater@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2013 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include beakerlib environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGES=${PACKAGE:-"python36 sqlite"}
PYTHON=${PYTHON:-python3}
REQUIRES=${REQUIRES:-sqlite}

rlJournalStart
  rlPhaseStartSetup
    #rlAssertRpm --all
    #rlAssertBinaryOrigin $PYTHON
    rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"
    rlRun "cp sqlite3-test.py $TmpDir" 0 "Copy test script to $TmpDir"
    rlRun "pushd $TmpDir"
  rlPhaseEnd

  rlGetTestState && {
    rlPhaseStartTest
        rlRun "$PYTHON sqlite3-test.py"
    rlPhaseEnd
  }

  rlPhaseStartCleanup
    rlRun "popd"
    rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
  rlPhaseEnd
rlJournalEnd
rlJournalPrintText
